import React, {Fragment, useState} from 'react';
import uuid from 'uuid/v4';

const Formulario = ({crearCita}) => {
    // Crear state de citas Model
    const [cita, updateCita] = useState(
        {
            mascota: '',
            propietario: '',
            fecha: '',
            hora: '',
            sintomas: ''
        }
    );
    //-----------------------------------------/

    //Error model--------------------------/
    const [error, updateError] = useState(
        false
    );
    //-----------------------------------------/

    // Funcion que se ejecuta cada que el usuario escribe un input
    const updateState = (event) => {
        updateCita({
            ...cita,
            [event.target.name]: event.target.value
        })
    }

    // Extraer los valores (Destructoring)
    const { mascota, propietario, fecha, hora, sintomas } = cita;

    //Cuando el usuario presiona agregar cita
    const submitCita = (event) => {
        event.preventDefault();

        //Validar
        if(
            mascota.trim() === '' ||
            propietario.trim() === '' ||
            fecha.trim() === '' ||
            hora.trim() === '' ||
            sintomas.trim() === ''
        ){
            updateError(true);
            return null;
        }
        //Eliminar mensaje de error
        updateError(false);

        //Asignar un id
        cita.id = uuid();

        //Crear la cita
        crearCita(cita);

        //Reiniciar el form
        updateCita({
            mascota: '',
            propietario: '',
            fecha: '',
            hora: '',
            sintomas: ''
        })
    }


    return (
        <Fragment>
            <h2>Crear Cita</h2>

            {
                error ?
                <p className="alerta-error">Todos los campos son obligatorios</p> : null
            }

            <form
            onSubmit={submitCita}
            >
                <label>Nombre de la mascota</label>
                <input
                    type="text"
                    name="mascota"
                    className="u-full-width"
                    placeholder="Escribe el nombre de tu mascota"
                    onChange={updateState}
                    value={mascota}
                    required={true}
                />
                <label>Nombre del dueño</label>
                <input
                    type="text"
                    name="propietario"
                    className="u-full-width"
                    placeholder="Escribe el nombre del dueño"
                    onChange={updateState}
                    value={propietario}
                    required={true}
                />
                <label>Fecha</label>
                <input
                    type="date"
                    // type="datetime-local"
                    name="fecha"
                    className="u-full-width"
                    onChange={updateState}
                    value={fecha}
                    required={true}
                />
                <label>Hora</label>
                <input
                    type="time"
                    name="hora"
                    className="u-full-width"
                    onChange={updateState}
                    value={hora}
                    required={true}
                />
                <label>Síntomas</label>
                <textarea
                    className="u-full-width"
                    name="sintomas"
                    onChange={updateState}
                    value={sintomas}
                    required={true}
                >
                </textarea>
                <button
                    type="submit"
                    className="u-full-width button-primary"
                >
                    Agregar Cita
                </button>
            </form>
        </Fragment>
    );
};

export default Formulario;
